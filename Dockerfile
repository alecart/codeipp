FROM python:3.9-slim

RUN apt-get -y update && \
    apt-get install -y --no-install-recommends git && \
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip install portray

COPY . /app

WORKDIR /app

EXPOSE 80

ENTRYPOINT ["portray", "server", "--host", "0.0.0.0", "--port", "80"]
